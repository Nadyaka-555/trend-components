;(function () {
    var leafletMap = {
        myMap: {},

        firstLoad: true,

        markersLayer: false,

        circlesLayer: false,

        icons: {
            default: L.icon({
                iconUrl: 'img/leaflet/lib/images/marker-icon.png',
                shadowUrl: 'img/leaflet/lib/images/marker-shadow.png',
                iconSize: [26, 35],
                iconAnchor: [12, 35],
                popupAnchor: [1, -34],
                tooltipAnchor: [16, -28],
                shadowSize: [41, 41]
            }),
            active: L.icon({
                iconUrl: 'img/leaflet/lib/images/marker-icon_active.png',
                shadowUrl: 'img/leaflet/lib/images/marker-shadow.png',
                iconSize: [26, 35],
                iconAnchor: [12, 35],
                popupAnchor: [1, -34],
                tooltipAnchor: [16, -28],
                shadowSize: [41, 41]
            }),
            selected: L.icon({
                iconUrl: 'img/leaflet/lib/images/marker-icon_select.png',
                shadowUrl: 'img/leaflet/lib/images/marker-shadow.png',
                iconSize: [26, 35],
                iconAnchor: [12, 35],
                popupAnchor: [1, -34],
                tooltipAnchor: [16, -28],
                shadowSize: [41, 41]
            })
        },

        circle: {
            radius: 600,
            stroke: false,
            fillColor: '#f00',
            fillOpacity: 0.4
        },

        /**
         * Инитим карту, добавляем контролы
         * @param {String} id - айди элемента
         * @param {Object} options - объект со свойствами: lat, lng, zoom, либо для плана: image_url, coordinates
         * @param {Array} points - массив активных точек на карте
         */
        init: function (id, options, points) {
            var self = this;
            var options = options || {};
            var mymap;

            options = self._checkOptions(options);

            if (options.image_url) {
                mymap = new L.Map(id, {
                    crs: L.CRS.Simple,
                    minZoom: -2,
                    maxZoom: 5,
                    doubleClickZoom: false,
                    scrollWheelZoom: false,
                    zoomControl: false,
                    keyboard: false,
                    center: [0, 0],
                    zoom: 0
                });

                var imgMap = new Image();

                imgMap.src = options.image_url;
                imgMap.onload = function () {
                    var bounds = new L.LatLngBounds(mymap.unproject([0, imgMap.height], 2), mymap.unproject([imgMap.width, 0], 2));

                    L.imageOverlay(options.image_url, bounds).bringToBack().addTo(mymap);

                    mymap.fitBounds(bounds);
                    mymap.setMaxBounds(bounds);
                };

                var polygon = L.polygon(options.coordinates, {
                    color: '#3388ff',
                    weight: 2,
                    fill: true,
                    fillColor: '#3388ff'
                });

                polygon.addTo(mymap);

                // костыль для навешивания стилей на карту в попапе
                L.DomUtil.addClass($(mymap.getContainer()).find('.leaflet-map-pane')[0], 'leaflet-map-pane_style_bottom');

            } else {
                // добавляем слои карты
                var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                });
                var yndx = new L.Yandex();

                // инитим карту
                mymap = new L.Map(id, {
                    zoomControl: false,
                    zoom: options.zoom || 15,
                    scrollWheelZoom: false
                });

                mymap.addLayer(layer);
                mymap.addControl(new L.Control.Layers({'OSM':layer, "Yandex":yndx}));

                if (options.lat && options.lng) {
                    mymap.setView([options.lat, options.lng]);
                }
            }

            // Добавляем контролы
            mymap.addControl(new L.Control.Fullscreen({
                position: 'topright',
                title: {
                    'false': 'Открыть на весь экран',
                    'true': 'Свернуть'
                },
                pseudoFullscreen: true
            }));

            mymap.addControl(new L.Control.ZoomFS({
                position: 'bottomright',
                zoomInTitle: 'Увеличить',
                zoomOutTitle: 'Уменьшить'
            }));

            L.control.polylineMeasure({
                position: 'topright'
            }).addTo(mymap);

            L.control.scale({
                position: 'bottomleft',
                imperial: false
            }).addTo(mymap);

            // добавляем специальный контрол "Показать/Скрыть все ЖК"
            self.AllObjControl = L.Control.extend({
                options: {
                    position: 'topright'
                },
                onAdd: function (map) {
                    var container = L.DomUtil.create('div', 'leaflet-control-allObjects active');
                    var controlText = L.DomUtil.create('div', 'leaflet-control-allObjects__text', container);

                    self.customControl = container;
                    self.customControlText = controlText;

                    controlText.innerHTML = 'Скрыть ЖК';
                    this.controlText = controlText;

                    L.DomEvent.on(container, 'click', this._toogleAllObjects, this);

                    return container;
                },
                _toogleAllObjects: function () {
                    var hideAll = 0;

                    if (L.DomUtil.hasClass(this._container, 'active')) {
                        L.DomUtil.removeClass(this._container, 'active');
                        this.controlText.innerHTML = 'Показать все ЖК';
                        hideAll = 0;
                    } else {
                        L.DomUtil.addClass(this._container, 'active');
                        this.controlText.innerHTML = 'Скрыть ЖК';
                        hideAll = 1;
                    }
                    self.markersLayer.eachLayer(function (marker) {
                        if (marker.options.active == false) {
                            marker.setOpacity(hideAll);
                        }
                    });
                }
            });

            self.myMap = mymap;

            self.block_cloud = $(self.myMap.getContainer()).prev();

            if (!options.image_url) {
                self.setMarkers(options, points);
            }
        },

        /**
         * Устанавливаем маркеры на карту
         * @param {Object} options - объект со свойствами: zoom, lat, lng
         * @param {Array} points - массив активных точек на карте
         */
        setMarkers: function (options, points) {
            var self = this;
            var mymap = self.myMap;
            var mapElem = mymap.getContainer();
            var markersLayer;

            self.points = points;
            self.options = self._checkOptions(options);

            self.block_cloud.length && self.block_cloud.removeClass('open');

            self.markersLayer && self.markersLayer.clearLayers();
            self.circlesLayer && self.circlesLayer.clearLayers();

            // костыль для навешивания стилей на карту в попапе
            if ($(mapElem).hasClass('object-flat__gallery__map')) {
                L.DomUtil.addClass($(mapElem).find('.leaflet-map-pane')[0], 'leaflet-map-pane_style_bottom');
            }

            if (mapElem.id === 'object-map_contacts') {
                // Содержимое попапчика для страницы контактов
                 var markerPopupContent = '<div class="leaflet-popup-content__content">' +
                    '<div class="h4 title">Тренд работает</div>' +
                    '<div id="bodyContent">' +
                    '<p>191123, Россия, Санкт-Петербург, Чайковского 26</p>' +
                    '<p>ПН-ПТ 09:00-20:00<br>СБ-ВС  10:00-18:00</p>' +
                    '<a href="#newton" rel="nofollow" onclick="$(\'#newton_callback_wrap\').show(); return false;" class="leaflet-popup-content__button leaflet-popup-content__button_block ' +
                    'leaflet-popup-content__button_primary leaflet-popup-content__button_arrow_right">Получить обратный звонок</a>' +
                    '</div>' +
                    '</div>';

                L.marker([self.options.lat, self.options.lng], {icon: self.icons.default})
                    .addTo(mymap)
                    .bindPopup(markerPopupContent)
                    .openPopup();

                if (self.options.cardinals && self.options.cardinals.length) {
                    self._setViewingAngle();
                }

            } else if (self.options.lat && self.options.lng) {
                L.marker([self.options.lat, self.options.lng], {icon: self.icons.default}).addTo(mymap);

                if (self.options.cardinals && self.options.cardinals.length) {
                    self._setViewingAngle();
                }

            } else if ('defaultSearchResults' in window) {

                var boundsArray = points.map(function (point) {
                    return [point.latitude, point.longitude];
                });

                self.myMap.addControl(new self.AllObjControl());
                self.myMap.fitBounds(boundsArray);

                if (defaultSearchResults.length == self.points.length) {
                    self.points = [];
                    self.options.firstLoad = true;
                    !$(self.customControl).hasClass('active') && $(self.customControl)
                        .addClass('active')
                        .find('.leaflet-control-allObjects__text')
                        .html('Скрыть ЖК');
                } else {
                    self.options.firstLoad = false;
                    $(self.customControl).hasClass('active') && $(self.customControl)
                        .removeClass('active')
                        .find('.leaflet-control-allObjects__text')
                        .html('Показать все ЖК');
                }

                var markersArr = defaultSearchResults.map(function (item) {
                    var active = self._equals(self.points, item.id, 'id');
                    var marker_pos = {
                        lat: item.latitude,
                        lng: item.longitude
                    };

                    return self._setMarker(marker_pos, item.id, active);
                });

                markersLayer = L.layerGroup(markersArr);
                markersLayer.addTo(self.myMap);
                self.markersLayer = markersLayer;
            }
        },

        /**
         * Создаем маркер
         * @param {Object} marker_pos - объект со свойствами lat, lng
         * @param {String} id - айди объекта маркера
         * @param {Boolean} active - активный ли маркер
         * @returns {*} - L.marker
         * @private
         */
        _setMarker: function (marker_pos, id, active) {
            var self = this;
            var markerOptions = {id: id, selected: false};

            if (active) {
                markerOptions.icon = self.icons.active;
                markerOptions.active = true;
            } else {
                markerOptions.icon = self.icons.default;
                (self.options.firstLoad == false) && (markerOptions.opacity = 0);
                markerOptions.active = false;
            }

            var marker = L.marker([marker_pos.lat, marker_pos.lng], markerOptions);

            L.DomEvent.on(marker, 'click', self._toggleObjectPopup, self);

            return marker;
        },

        /**
         * Обработчик клика по маркеру - открываем попап и меняем иконку
         * @param {Object} event
         * @private
         */
        _toggleObjectPopup: function (event) {
            L.DomEvent.preventDefault(event);
            L.DomEvent.stopPropagation(event);

            var self = this;
            var marker = event.target;
            var id = marker.options.id;

            if (!self.block_cloud.hasClass('object__map__cloud')) {
                return;
            }

            L.DomEvent.on(self.block_cloud.find('.object__map__cloud__close')[0], 'click', self._closeObjectPopup, self);

            if (marker.options.selected) {
                self.block_cloud.removeClass('open');
            } else {
                self._getObjectInfo(id);
            }

            self.markersLayer.eachLayer(function (marker) {
                if ((marker.options.id !== id) && marker.options.selected) {
                    self._changeMarkerIcon(marker);
                }
            });

            self._changeMarkerIcon(marker);
        },

        /**
         * Обработка клика по закрывающему крестику попапа объекта
         * @param {Object} event
         * @private
         */
        _closeObjectPopup: function (event) {
            L.DomEvent.preventDefault(event);
            L.DomEvent.stopPropagation(event);

            var self = this;
            var id = +self.block_cloud.attr('data-id');

            self.block_cloud.removeClass('open');

            self.markersLayer.eachLayer(function (marker) {
                if (marker.options.id === id) {
                    self._changeMarkerIcon(marker);
                }
            });
        },

        /**
         * Функция получения данных объекта на карте и подставления данных в попапчик
         * @param {String} id - айди объекта
         * @private
         */
        _getObjectInfo: function (id) {
            var self = this;

            self.block_cloud.find('.js-preview').attr('src', 'img/default-clear.svg');

            $.ajax({
                type: "GET",
                url: apiUrl + 'blocks/' + id + '/map',
                crossDomain: true,
                success: function (response) {
                    var data = response.data;

                    if (data.errors == null) {
                        self.block_cloud.attr('data-id', id);
                        self.block_cloud.find('.js-deadline').html('<span>Срок сдачи</span> ' + data.deadline);
                        self.block_cloud.find('.js-address').html(data.region.name + ', ' + data.address);
                        self.block_cloud.find('.js-preview').attr('src', data.image);
                        self.block_cloud.find('.js-object-link').attr('href', 'object/' + data.guid);
                        self.block_cloud.find('.js-title').html(data.name);
                        self.block_cloud.find('.js-price').html('');

                        for (var i = 0; i < data.apartmentsMinPrices.length; i++) {
                            self.block_cloud.find('.js-price').append('<div class="grid top">' +
                                '<div class="cell-md-4 search__results__item__summary__prices__type">' + data.apartmentsMinPrices[i].rooms + '</div>' +
                                '<div class="cell-md-8 search__results__item__summary__prices__num">' + data.apartmentsMinPrices[i].price + ' руб.</div>' +
                                '</div>'
                            );
                        }

                        self.block_cloud.find('.js-subway').html('');

                        for (var i = 0; i < data.subway.length; i++) {
                            var icons;

                            if (data.subway[i].distance_type == 'пешком') {
                                icons = 'icons--man';
                            } else {
                                icons = 'icons--bus';
                            }

                            self.block_cloud.find('.js-subway').append(
                                '<tr><td>' +
                                '<span class="search__results__item__summary__place">' +
                                '<i class="icons icons__metro icons__metro--' + data.subway[i].line_number + '">' +
                                '</i> ' + data.subway[i].name + '</span>' +
                                '<span class="search__results__item__summary__place">' +
                                '<i class="icons ' + icons + '"></i> ' + data.subway[i].distance_timing + ' мин.' +
                                '</span>' +
                                '</td></tr>'
                            );
                        }

                        self.block_cloud.addClass('open');
                    }
                },
                error: function (err) {
                    console.info('Запрос не удался: ', err);
                }
            });
        },

        /**
         * Смена иконки маркера при клике на него
         * @param {Object} marker - L.Marker
         * @private
         */
        _changeMarkerIcon: function (marker) {
            var self = this;
            var selected = marker.options.selected;

            if (selected) {
                marker.setIcon(self.icons[(marker.options.active) ? 'active' : 'default']);
            } else {
                marker.setIcon(self.icons.selected);
            }

            marker.options.selected = !selected;
        },

        /**
         * Проверяем наличие id маркера результатов в объекте points
         * @param {Object} obj1
         * @param {String} param
         * @param {String} key
         * @returns {number} - 0 или 1
         * @private
         */
        _equals: function (obj1, param, key) {
            var count = 0;

            for (var i in obj1) {
                if (obj1[i][key] == param) {
                    count++;
                }
            }

            return count;
        },

        /**
         * Проверяем, в каком типе пришли options
         * @param {Object || String} options
         * @returns {Object} объект options
         * @private
         */
        _checkOptions: function (options) {
            if (typeof options == "string") {
                try {
                    options = JSON.parse(options);
                } catch (err) {
                    console.log('Неверный формат входящих данных для карты leaflet');
                }
            }

            return options;
        },

        /**
         * Рисуем сектора обзора в 45 градусов согласно массиву полученных градусов
         * @private
         */
        _setViewingAngle: function () {
            var self = this;
            var angleArr = self._getAngleArray();
            var circlesLayer;
            var circleArr = angleArr.map(function (degree) {
                return L.circle([self.options.lat, self.options.lng], self.circle).setDirection(degree, 45);
            });

            circlesLayer = L.featureGroup(circleArr);
            circlesLayer.addTo(self.myMap);
            self.circlesLayer = circlesLayer;
        },

        /**
         * Получаем итоговый массив градусов для отрисовки секторов обзора
         * @returns {Array} массив углов
         * @private
         */
        _getAngleArray: function () {
            var self = this;
            var cardinalsArray = self.options.cardinals.reduce(function (resultArr, item, index, arr) {
                if (!resultArr.length) {
                    resultArr.push(item);

                    return resultArr;
                } else {
                    var lastItem = resultArr[resultArr.length -1]; // последний элемент массива

                    if (item - lastItem >= 90) {
                        resultArr.push(item);
                        return resultArr;
                    } else {
                        var newItem = parseInt((item + lastItem) / 2);

                        resultArr.pop();
                        resultArr.push(newItem);
                        if (index != arr.length - 1) {
                            resultArr.push(item);
                        }
                        return resultArr;
                    }
                }
            }, []);

            return cardinalsArray;
        }
    };

    window.leafletMap = leafletMap;

}());

$(function () {
    var leafletContainer = $('.leaflet-map');

    if (leafletContainer.length) {
        leafletContainer.each(function (ind, elem) {
            var options = $(elem).data('leaflet') || '';

            leafletMap.init(elem.id, options, []);
        });
    }
});
