'use strict';

var gulp = require('gulp'),
    cssmin = require('gulp-minify-css'), // нужен для сжатия CSS кода
    sass = require('gulp-sass'), // для компиляции нашего SASS кода (можно использовать LESS)
    sourcemaps = require('gulp-sourcemaps'), // возьмем для генерации css sourscemaps, которые будут помогать нам при отладке кода
    imagemin = require('gulp-imagemin'), // для сжатия картинок
    urlAdjuster = require('gulp-css-url-adjuster'), // смена адреса
    pngquant = require('imagemin-pngquant'), // дополнения к gulp-imagemin, для работы с PNG
    browserSync = require("browser-sync"), // с помощью этого плагина мы можем легко развернуть локальный dev сервер с блэкджеком и livereload
    reload = browserSync.reload,
    concat = require('gulp-concat'),
    debug = require('gulp-debug'),
    browserify = require('browserify'),
    gutil = require('gulp-util'),
    tap = require('gulp-tap'),
    buffer = require('gulp-buffer'),
    uglify = require('gulp-uglify'),
    cssimport = require("gulp-cssimport");

var path = {
    build: {
        js: './trend_components/build/js/',
        js_static: 'static/js',
        css_static: 'static/css/',
        img_static: 'static/img/'
    },
    src: {
        js_blocks: ['./trend_components/blocks/**/*.js', '!./trend_components/blocks/**/lib/*.js'],
        js_lib: './trend_components/blocks/**/lib/lib.js',
        style: ['./trend_components/blocks/**/lib/lib.scss'],
        img: './trend_components/blocks/**/lib/images/*.*'
    }
};

gulp.task('blocks', function () {
    return gulp.src(path.src.js_blocks) // Найдем клиентские файлы компонентов
        .pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(debug())
        .pipe(concat('blocks.js'))
        .pipe(debug())
        .pipe(sourcemaps.write())
        .pipe(debug())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('lib', function () {
    return gulp.src(path.src.js_lib, {read: false}) // no need of reading file because browserify does
        .pipe(tap(function (file) { // transform file objects using gulp-tap plugin
            gutil.log('bundling ' + file.path);
            file.contents = browserify({ // replace file contents with browserify's bundle stream
                entries: file.path,
                debug: true
            }).bundle();
        }))
        .pipe(buffer()) // transform streaming contents into buffer contents (because gulp-sourcemaps does not support streaming contents)
        .pipe(sourcemaps.init({loadMaps: true})) // load and init sourcemaps
        .pipe(debug())
        .pipe(concat('components.js'))
        // .pipe(uglify())
        .pipe(sourcemaps.write('./')) // write sourcemaps
        .pipe(debug())
        .pipe(gulp.dest(path.build.js_static));
});

gulp.task('style', function () {
    return gulp.src(path.src.style)
        .pipe(debug())
        .pipe(sourcemaps.init()) // Инициализируем sourcemap
        .pipe(debug())
        .pipe(sass().on('error', sass.logError)) // Скомпилируем
        .pipe(debug())
        .pipe(cssimport({
            includePaths: ['./trend_components/node_modules/'],
            matchPattern: "*.css" // process only css
        }))
        .pipe(concat('components.css'))
        .pipe(debug())
        .pipe(urlAdjuster({
            replace: ['../../', '../img/']
        }))
        // .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(debug())
        .pipe(gulp.dest(path.build.css_static)); // Выплюнем готовый файл в build
});

gulp.task('image', function () {
    return gulp.src(path.src.img) // Выберем картинки компонентов
        .pipe(imagemin({ // Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(debug())
        .pipe(gulp.dest(path.build.img_static)) // И бросим в build
        .pipe(reload({stream: true}));
});

module.exports = 'trend_components';
